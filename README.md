## Работа с текстом

- очистка стилей
- удаление html тегов

## install
```bash
composer require domatskiy/text-util
```

###use
```php
$text = '<p style="padding:0px;"><strong style="padding:0;margin:0;">hello</strong></p>';
$cleanText = CleanHtml::delStyleAttribute($text);
echo $cleanText; // <p><strong>hello</strong></p>

$cleanText = CleanHtml::delTags($text, 'strong, div');
echo $cleanText; // <p style="padding:0px;">hello</p>
```
