<?php

namespace Domatskiy\Text;

class CleanHtml
{
    /**
     * Удаление стилей из html
     * @param string $text
     * @return string|string[]|null
     */
    public static function delStyleAttribute(string $text)
    {
        return preg_replace('~ style="[^"]*"~i', '', $text);
    }

    /**
     * Удажение тегов из текста
     * @param string $txt
     * @param string $tag  Можно передавать более одного тега, разделяя их запятой (можно с пробелом или без него).
     * @return string|string[]|null
     */
    public static function delTags(string $txt, string $tag) {
        $tags = explode(',', $tag);

        do {
            $tag = array_shift($tags);
            $txt = preg_replace("~<($tag)[^>]*>|(?:</(?1)>)|<$tag\s?/?>~x", '', $txt);
        } while (!empty($tags));

        return $txt;
    }
}
