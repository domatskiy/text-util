<?php

namespace Domatskiy\Tests\Unit;

use Domatskiy\Tests\TestCase;
use Domatskiy\Text\CleanHtml;

class HtmlTest extends TestCase
{
    public function testHtml()
    {
        $text = '<p style="padding:0px;"><strong style="padding:0;margin:0;">hello</strong></p>';

        $cleanText = CleanHtml::delStyleAttribute($text);
        $this->assertTrue(strpos($cleanText, 'style') === false);
        $this->assertTrue(strpos($cleanText, 'p') !== false, 'tag "p" not found: '.$cleanText);
        $this->assertTrue(strpos($cleanText, 'strong') !== false, 'tag "strong" not found: '.$cleanText);

        $cleanText = CleanHtml::delTags($text, 'strong');
        $this->assertTrue(strpos($cleanText, 'style') !== false, 'attr "style" not found: '.$cleanText);
        $this->assertTrue(strpos($cleanText, 'p') !== false, 'tag "p" found: '.$cleanText);
        $this->assertTrue(strpos($cleanText, 'strong') === false, 'tag "strong" found: '.$cleanText);
    }
}
